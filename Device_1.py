# For setting up the MQTT Client.
import os

# For discovering the AWS Greengrass Core over the Internet.
import sys

# For sleeping/delays.
import time

# For getting timestamps with hour, minutes, seconds, and milliseconds.
from datetime import datetime

# For setting up the MQTT Client.
import uuid

# For sending messages.
import json

# For generating random values.
import random

# For trig and power methods.
import math

# For AWS SDK for Python.
import boto3

# For setting up the MQTT Client.
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient

# For discovering the AWS Greengrass Core over the Internet.
from AWSIoTPythonSDK.core.greengrass.discovery.providers import DiscoveryInfoProvider
from AWSIoTPythonSDK.exception.AWSIoTExceptions import DiscoveryInvalidRequestException
from AWSIoTPythonSDK.core.protocol.connection.cores import ProgressiveBackOffCore

###############################################################################
###     THE AWS IOT ENDPOINT    ###
###############################################################################

# "INSERT YOUR AWS ENDPOINT HERE"
host = "a1bbeux7bd6r8d.iot.us-west-2.amazonaws.com"

###############################################################################
###     THE NAME OF THE GREENGRASS DEVICE    ###
###############################################################################

# The name of the Greengrass device.
clientId = "device_ubuntu"
thingName = clientId

###############################################################################
###     THE PATHS TO THE CERTIFICATES AND KEYS      ###
###############################################################################

# "INSERT THE PORTION OF THE FILENAME FOR THE TAR.GZ FILE THAT COMES BEFORE -setup.tar.gz"
cert_name = clientId

# The location of the AWS IoT root certificate
rootCAPath = os.path.expanduser('~') + "/ecep_client_cpu/ecep_client/greengrass_device/certs/root.ca.pem"

# The location of the device's certificate
certificatePath = os.path.expanduser('~') + "/ecep_client_cpu/ecep_client/greengrass_device/certs/" + cert_name + ".cert.pem"

# The location of the device's private key.
privateKeyPath = os.path.expanduser('~') + "/ecep_client_cpu/ecep_client/greengrass_device/certs/" + cert_name + ".private.key"

# If the credentials are missing.
if not certificatePath or not privateKeyPath:
    parser.error("Missing credentials for authentication.")
    exit(2)

###############################################################################
###     INFO RELATED TO COMMUNICATION FOR THE GREENGRASS DEVICE    ###
###############################################################################

# The MQTT topic this device will publish to when there is an emergency.
topic_publisher = "vehicles/alerts"

# The MQTT topic this device will subscribe to.
topic_subscriber = "vehicles/alerts"

# The mode for this device is both since it is sending data and receiving data.
mode = "both"

###############################################################################
###     OTHER VARIABLES    ###
###############################################################################

road = {}
location = {}
vehicle_speed = 0
tpms_value_alert_published = False

# Use the Haversine forumula to get the distance between two points that use GPS coordinates
# (psi is for the latitude values and lamb is for longitude).
def distance(psi_1, lamb_1, psi_2, lamb_2):

    R = 6371 * math.pow(10, 3)

    psi_1_rad = math.radians(psi_1)

    psi_2_rad = math.radians(psi_2)

    delta_psi_rad = math.radians(psi_2 - psi_1)

    delta_lamb_rad = math.radians(lamb_2 - lamb_1)

    a = math.pow(math.sin(delta_psi_rad/2), 2) + (math.cos(psi_1_rad) * math.cos(psi_2_rad) * math.pow( math.sin(delta_lamb_rad/2), 2 ))

    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))

    d = R * c

    # Return the distance in meters.
    return d

# Calculate the stopping distance (reaction distance + braking distance) using speed (mph).
def stopping_distance(speed):

    #Convert mph to km/h.
    speed_kmph = speed * 1.60934

    # Convert km/h to m/s.
    velocity = ( (speed_kmph * 1000)/(60) )/60

    # Assume the reaction time in seconds is 1.
    r = 1

    # Calculate the reaction distance (result is in meters).
    reaction_distance = (speed_kmph * r)/3.6

    # Assume the coefficient of friction is 0.8 for dry asphalt.
    f = 0.6

    # 9.8 m/s^2
    g = 9.8

    # Calculate the braking distance (result is in meters).
    braking_distance = math.pow(speed_kmph, 2)/(2 * f * g)

    # Calculate the stopping distance.
    stopping_distance = reaction_distance + braking_distance

    # Return the distance in meters.
    return stopping_distance

# General message notification callback.
def customOnMessage(message):

    message_converted = json.loads(message.payload)

    # If the vehicle that sent the message is not the vehicle that received the message.
    if not message_converted['vehicle'] == clientId:

        secondary_vehicle_heading = message_converted['road']['heading']

        # If the two vehicles have the same heading.
        if secondary_vehicle_heading == road['heading']:

            # Get the other vehicle's location.
            secondary_vehicle_location = message_converted['location']

            if road['heading'] == "North":
                if location['latitude'] < secondary_vehicle_location['latitude']:
                    compare_distances = True

            elif road['heading'] == "South":
                if location['latitude'] > secondary_vehicle_location['latitude']:
                    compare_distances = True

            elif road['heading'] == "East":
                if location['longitude'] < secondary_vehicle_location['longitude']:
                    compare_distances = True

            elif road['heading'] == "West":
                if location['longitude'] > secondary_vehicle_location['longitude']:
                    compare_distances = True

        # If the distances need to be compared.
        if compare_distances:

            # Get the distance between two points that use GPS coordinates.
            distance_between_vehicles = distance(
                secondary_vehicle_location['latitude'], secondary_vehicle_location['longitude'],
                location['latitude'], location['longitude'])

            distance_to_stop = stopping_distance(vehicle_speed)

            # Send an alert.
            if distance_between_vehicles < distance_to_stop:

                warning = datetime.now().strftime("%D %H:%M:%S.%f")

                warning += " "

                warning += message_converted['vehicle']

                warning += " may be disabled in front of you! Please slow down."

                print(warning)

###############################################################################
###     DISCOVER THE GREENGRASS CORE    ###
###############################################################################

MAX_DISCOVERY_RETRIES = 10
GROUP_CA_PATH = "./groupCA/"

# Progressive back off core
backOffCore = ProgressiveBackOffCore()

# For discovering the AWS Greengrass Core over the Internet.
discoveryInfoProvider = DiscoveryInfoProvider()
discoveryInfoProvider.configureEndpoint(host)
discoveryInfoProvider.configureCredentials(rootCAPath, certificatePath, privateKeyPath)
discoveryInfoProvider.configureTimeout(10)  # 10 sec

# Try to discover the GGC for this group.
retryCount = MAX_DISCOVERY_RETRIES
discovered = False
groupCA = None
coreInfo = None
while retryCount != 0:
    try:
        discoveryInfo = discoveryInfoProvider.discover(thingName)
        caList = discoveryInfo.getAllCas()
        coreList = discoveryInfo.getAllCores()

        # We only pick the first ca and core info
        groupId, ca = caList[0]
        coreInfo = coreList[0]
        print("Discovered GGC: %s from Group: %s\n" % (coreInfo.coreThingArn, groupId))

        print("Now we persist the connectivity/identity information...\n")
        groupCA = GROUP_CA_PATH + groupId + "_CA_" + str(uuid.uuid4()) + ".crt"
        if not os.path.exists(GROUP_CA_PATH):
            os.makedirs(GROUP_CA_PATH)
        groupCAFile = open(groupCA, "w")
        groupCAFile.write(ca)
        groupCAFile.close()

        discovered = True
        print("Now proceed to the connecting flow...\n")
        break
    except DiscoveryInvalidRequestException as e:
        print("Invalid discovery request detected!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % e.message)
        print("Stopping...")
        break
    except BaseException as e:
        print("Error in discovery!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % e.message)
        retryCount -= 1
        print("\n%d/%d retries left\n" % (retryCount, MAX_DISCOVERY_RETRIES))
        print("Backing off...\n")
        backOffCore.backOff()

# If the GGC was not found.
if not discovered:
    print("Discovery failed after %d retries. Exiting...\n" % (MAX_DISCOVERY_RETRIES))
    sys.exit(-1)

# Iterate through all connection options for the core and use the first successful one.
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureCredentials(groupCA, privateKeyPath, certificatePath)
myAWSIoTMQTTClient.onMessage = customOnMessage

# Connect to the MQTT client on the GGC.
connected = False
for connectivityInfo in coreInfo.connectivityInfoList:
    currentHost = connectivityInfo.host
    currentPort = connectivityInfo.port
    print("Trying to connect to core at %s:%d\n" % (currentHost, currentPort))
    myAWSIoTMQTTClient.configureEndpoint(currentHost, currentPort)
    try:
        myAWSIoTMQTTClient.connect()
        connected = True
        break
    except BaseException as e:
        print("Error in connect!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % e.message)

# If connection to the MQTT client on the GGC failed.
if not connected:
    print("Cannot connect to core %s. Exiting..." % coreInfo.coreThingArn)
    sys.exit(-2)

else:
	print("Connected.\n")

# Successfully connected to the core
if mode == 'both' or mode == 'subscribe':
    myAWSIoTMQTTClient.subscribe(topic_subscriber, 0, None)
time.sleep(2)

# Get the the road that the vehicle is currently traveling on.
def get_vehicle_road_info():

    global road
    road = {}
    road['name'] = "101"
    road['heading'] = "North"
    road['speed_limit'] = 65

    return road

# Get the vehicle's speed (in MPH).
def get_vehicle_speed():
    return 0

# Get the vehicle's location.
def get_vehicle_location():

    # This is about 300 ft (100 m) north of the Tully Rd (Eastbound) on-ramp to US 101 N.
    global location
    location = {}
    location['latitude'] = 37.320246
    location['longitude'] = -121.833072

    return location

# Get the TPMS value.
def get_tpms_value():
    return ( bool( random.getrandbits(1) ) )

while True:

    # Get the road that the vehicle is currently traveling on.
    vehicle_road_info = get_vehicle_road_info()

    # Get the vehicle's location.
    vehicle_location = get_vehicle_location()

    # Get the vehicle's speed.
    vehicle_speed = get_vehicle_speed()

    # Get the TPMS warning value (True or False).
    tpms_value = get_tpms_value()

    # True = warning or False = no warning.
    if ( tpms_value and (vehicle_speed <= vehicle_road_info['speed_limit'] - 10) ):

        # If the alert has not been published yet.
        if not tpms_value_alert_published:

            # Send the vehicle information to other vehicle via the AWS IoT Cloud.
            if mode == 'both' or mode == 'publish':
                message = {}
                message['time'] = datetime.now().strftime("%D %H:%M:%S.%f")
                message['location'] = vehicle_location
                message['road'] = vehicle_road_info
                message['speed'] = vehicle_speed
                message['sensor'] = "TPMS"
                message['vehicle'] = clientId
                messageJson = json.dumps(message)
                myAWSIoTMQTTClient.publish(topic_publisher, messageJson, 0)
                print('Published topic %s: %s\n' % (topic_publisher, messageJson))
                tpms_value_alert_published = True

    # Clear the alert.
    else:
        tpms_value_alert_published = False

    time.sleep(5)
