CMPE 295 Device 1

Getting Started

This program is part of the use case and simulates a vehicle with an issue with
the TPMS sensor (i.e. a flat tire).

Prerequisites

The client must have Git installed to clone the repo.

Installing

Clone the repo then run the following command:

sudo python Device_1.py

Versioning

v1.0

Authors

Sona Bhasin
Kanti Bhat
Johnny Nigh
Monica Parmanand
